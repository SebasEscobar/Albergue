<!DOCTYPE html>
<html lang="es" xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Albergue Animal | Categorias</title>
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="robots" content="nofollow" />
	<link rel="stylesheet" type="text/css" href="../css/am-styles.css" />
	<link rel="stylesheet" type="text/css" href="../css/medias.css" />
	<link rel="stylesheet" type="text/css" href="../css/fuentes.css" />


	<link rel="stylesheet" href="../icons/style.css">


	<link rel="stylesheet" type="text/css" href="../css/fonts.css" />
	<script language="JavaScript" src="../js/jquery-1.11.3-jquery.min.js" type="text/javascript"></script>
	<script language="JavaScript" src="../js/bootstrap.min.js" type="text/javascript"></script>
	<script language="JavaScript" src="../js/SGE-script.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function(){
		});
	</script>
</head>
<body>
	<header>
		<div class="menu_bar">
		<a  class="bt-menu"><img src="../images/logo.png" width="40px" alt=""><h1 class="titulo_header">Categorias</h1><span class="icon-list2"></span></a>
		</div>
		 	<nav>
				<ul>
					<li><a class="estado1" href="perfil.php">Perfil</a></li>
					<li><a class="estado2" href="h_adoptados.php">Historial de adoptados</a></li>
					<li><a class="estado1" href="reportar_caso.php">Reportar un caso</a></li>
					<li><a class="estado1" href="campañas.php">Campañas</a></li>
					<li><a class="estado1" href="tips.php">Tips</a></li>
					<li><a class="estado1" href="#">Cerrar sesión</a></li>
				</ul>
			</nav>
		</header>

		<h1 class="titulo">¿A quién quiéres adoptar?</h1>
		<div class="categoria">
			<img class="img_categoria" src="../images/img_muestra1.jpg" alt="">
			<div class="detalle_categoria">
				<h1 class="titulo_categorias">Canino</h1>
			</div>
		</div>
		<div class="categoria">
			<img class="img_categoria" src="../images/cat1.jpg" alt="">
			<div class="detalle_categoria">
				<h1 class="titulo_categorias">Felino</h1>
			</div>
		</div>
		<div class="categoria">
			<img class="img_categoria" src="../images/horse1.jpg" alt="">
			<div class="detalle_categoria">
				<h1 class="titulo_categorias">Equino</h1>
			</div>
		</div>




	<script src="http://code.jquery.com/jquery-latest.js"></script>
	<script src="../js/menu.js"></script>
</body>
</html>