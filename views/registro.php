<!DOCTYPE html>
<html lang="es" xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Albergue Animal | Registro</title>
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta name="robots" content="nofollow" />
	<link rel="stylesheet" type="text/css" href="../css/SGE-styles.css" />
	<link rel="stylesheet" type="text/css" href="../css/medias.css" />
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="../css/fonts.css" />
	<script language="JavaScript" src="../js/jquery-1.11.3-jquery.min.js" type="text/javascript"></script>
	<script language="JavaScript" src="../js/bootstrap.min.js" type="text/javascript"></script>
	<script language="JavaScript" src="../js/validation.min.js" type="text/javascript"></script>
	<script language="JavaScript" src="../js/validate-form-usuarios.js" type="text/javascript"></script>
	<script language="JavaScript" src="../js/SGE-script.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#btn-registrar').click(function(){
				var nombres = $('input[name="nombres"]').val();
				var apellidos = $('input[name="apellidos"]').val();
				var documento = $('input[name="documento"]').val();
				var genero = $('input[name="genero"]').val();
				var estrato = $('input[name="estrato"]').val();
				var celular = $('input[name="celular"]').val();
				var user = $('input[name="email"]').val();
				var pass = $('input[name="password"]').val();
				postCrear(nombres,apellidos,documento,genero,estrato,celular,user,pass);
			});
		});
		function postCrear(nom,ape,doc,gen,est,cel,user,pass){
			$.ajax({
				method: "POST",
				url: "http://albergue-municipal.dev/usuarios/crear",
				data: { 
					nombres: nom, 
					apellidos: ape, 
					documento: doc,
					genero: gen,
					estrato: est,
					celular: cel,
					usuario: user, 
					contraseña: pass 
				}
			})
				.done(function( msg ) {
					var redirec = '../index.php';
					window.location.href = redirec;
				});
		}
	</script>
</head>
<body class="col-xs-12 view-login">
<?php

require_once '../php/dbconfig.php';

/*if($_POST){
	$user_nombres = $_POST['nombres'];
	$user_apellidos = $_POST['apellidos'];
	$user_documento = $_POST['documento'];
	$user_genero = $_POST['genero'];
	$user_estrato = $_POST['estrato'];
	$user_celular = $_POST['celular'];
	$user_email = $_POST['email'];
	$user_password = $_POST['password'];
	$user_level = 1;
	$fecha_ingreso =date('Y-m-d H:i:s');
  
	try{ 
	  
	    $stmt = $db_con->prepare("SELECT * FROM usuarios WHERE email=:email");
	    $stmt->execute(array(":email"=>$user_email));
	    $count = $stmt->rowCount();
   
    	if($count==0){
	    	$stmt = $db_con->prepare("INSERT INTO usuarios(id,nombres,apellidos,documento,genero,estrato,celular,email,password,level,fecha_ingreso) VALUES(null, :nombres, :apellidos, :documento, :genero, :estrato, :celular, :email, :pass, :level, :jdate)");
		    $stmt->bindParam(":nombres",$user_nombres);
		    $stmt->bindParam(":apellidos",$user_apellidos);
		    $stmt->bindParam(":documento",$user_documento);
		    $stmt->bindParam(":genero",$user_genero);
		    $stmt->bindParam(":estrato",$user_estrato);
		    $stmt->bindParam(":celular",$user_celular);
		    $stmt->bindParam(":email",$user_email);
		    $stmt->bindParam(":pass",$user_password);
		    $stmt->bindParam(":level",$user_level);
		    $stmt->bindParam(":jdate",$fecha_ingreso);
     
	    	if($stmt->execute()){
	     		echo "Usuario Registrado";
	    	}else{
	     		echo "No se ha podido registrar!";
	    	}
    	}else{
     		echo "1"; //  not available
    	}
    
    }
  	catch(PDOException $e){
    	echo $e->getMessage();
  	}
}*/

?>
	<div class="img-log">
		<img src="../images/background.jpg">
	</div>
	<div class="login">
		<div class="logo">
			<i class="glyphicon glyphicon-home"></i>
		</div>
		<!-- Validacion y envio de datos desactivados  -->
		<form id="frm-registro" class="frm-inputs" method="post" accept-charset="utf-8">
			<div id="error">
	        	<!-- error will be showen here ! -->
	        </div>
			<input type="text" name="nombres" placeholder="nombres">
			<input type="text" name="apellidos" placeholder="apellidos">
			<input type="number" name="documento" placeholder="documento">
			<label class="line-v">Genero: </label>
			<label>Macho</label>
			<input type="radio" name="genero" value="1">
			<label>Hembra</label>
			<input type="radio" name="genero" value="0">
			<input type="number" name="estrato" min="1" max="5" placeholder="Estrato">
			<input type="number" name="celular" placeholder="Telefono">
			<input type="email" name="email" placeholder="Correo electronico">
			<input type="password" name="password" placeholder="Contraseña" id="password">
			<input type="password" name="cpassword" placeholder="Confirmar contraseña"><br><br>
			<!-- Cambiar type=submit para validacion y registro -->
			<button type="button" class="btn btn-default" name="btn-save" id="btn-submit">Registrarme</button>
		</form>
	</div>
</body>
</html>