<!DOCTYPE html>
<html lang="es" xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Albergue Animal | Login</title>
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta name="robots" content="nofollow" />
	<link rel="stylesheet" type="text/css" href="css/SGE-styles.css" />
	<link rel="stylesheet" type="text/css" href="css/medias.css" />
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="css/fonts.css" />
	<script language="JavaScript" src="js/jquery-1.11.3-jquery.min.js" type="text/javascript"></script>
	<script language="JavaScript" src="js/bootstrap.min.js" type="text/javascript"></script>
	<script language="JavaScript" src="js/SGE-script.js" type="text/javascript"></script>
</head>
<body class="col-xs-12 view-login">
	<div class="img-log">
		<img src="images/background.jpg">
	</div>
	<div class="login">
		<div class="logo">
			<i class="glyphicon glyphicon-home"></i>
		</div>
		<form id="frm-Login" class="frm-inputs">
			<input type="text" name="email" placeholder="Correo">
			<input type="password" name="password" placeholder="Contraseña">
			<button type="button" id="btn-ingresar">Entrar</button>
		</form>
	</div>
	<a href="views/registro.php" class="line-v">¿No haces parte?</a>
</body>
</html>