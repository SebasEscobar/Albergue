$(document).ready(function(){
    $('#btn-ingresar').click(function(){
        var user = $('input[name="email"]').val();
        var pass = $('input[name="password"]').val();
        if ( user != '' && pass != '' ){
            var redirect = 'views/categorias.php';
            window.location.href = redirect;
        }
        if (user == '') {
            $('input[name="email"]').attr('style', 'border-color:#d50000;background-color:rgba(213,0,0,0.2)');
        }
        if (pass == ''){
            $('input[name="password"]').attr('style', 'border-color:#d50000;background-color:rgba(213,0,0,0.2)');
        }
    });
    $('#btn-submit').click(function(){
        var redirect = 'categorias.php';
        window.location.href = redirect;
    });
});